function onClick() {
    let result = document.getElementById("result");
    let price = document.getElementById("price");
    let count = document.getElementById("count");
    result.value = price.value * count.value;
    alert(result.value);
}

window.addEventListener('DOMContentLoaded', function () {
    console.log("DOM fully loaded and parsed");
    let b = document.getElementById("button1");
    b.addEventListener("click", onClick);
});

